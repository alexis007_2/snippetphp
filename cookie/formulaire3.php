<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<form action="traitement3.php" method="get">
		<div class="form-group">
			<label for="nom">nom</label>
			<input type="text" class="form-control w-25" id="nom" name="nom">
		</div>
		<div class="form-group">
			<label for="prenom">prenom</label>
			<input type="text" class="form-control w-25"  name="prenom" id="prenom">
		</div>
		<div class="form-group">
			<label for="age">age</label>
			<input type="text" class="form-control w-25" name="age" id="age">
		</div>		
		<button type="submit" class="btn btn-primary">Envoyer</button>
	</form>

	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/@popperjs/core@2"></script>
</body>
</html>