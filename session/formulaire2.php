<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<form action="traitement2.php" method="post">
		<div class="form-group">
			<label for="login">login</label>
			<input type="text" class="form-control w-25"  name="login" id="login">
		</div>
		<div class="form-group">
			<label for="pass">Mot de passe</label>
			<input type="password" class="form-control w-25" name="pass" id="pass">
		</div>		
		<button type="submit" class="btn btn-primary">Envoyer</button>
	</form>

	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/@popperjs/core@2"></script>
</body>
</html>